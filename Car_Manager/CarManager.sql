
Use Carr
Go
create table Brand(
idBrand nvarchar(50) not null primary key,
Brand nvarchar(100) not null,
)
GO
create table Car
(
 idCar int not null primary key,
 idBrand nvarchar(50) not null,
 Name nvarchar(50) not null,
 Price int not null,
 Color nvarchar(50) not null,
 constraint fk_Brand FOREIGN KEY (idBrand)
    REFERENCES Brand(idBrand)
 )
