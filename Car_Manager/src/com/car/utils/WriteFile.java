package com.car.utils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import com.car.model.Car;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class WriteFile {

	private final static String URL = "C:\\Users\\ASUS\\Desktop\\jsonFileTest\\";

	public static void ObjectJson(List<Car> list) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
		writer.writeValue(new File(URL + "Car" + LocalDate.now().toString() + "-" + LocalTime.now().getHour() + "-"
				+ LocalTime.now().getMinute() + "-" + LocalTime.now().getSecond() + ".json"), list);
	}

}
