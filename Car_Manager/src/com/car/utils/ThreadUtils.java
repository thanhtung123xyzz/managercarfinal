package com.car.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.car.dao.CarDao;
import com.car.model.Car;

public class ThreadUtils implements Runnable {
	private static List<Car> listCar = null;
	CarDao dao = new CarDao();

	@Override
	public void run() {
		for (int i = 0; true; i++) {
			try {
				Thread.sleep(10000);
				List<Car> list = null;
				try {
					list = dao.select();
				} catch (Exception e) {
					Logger.getLogger(ThreadUtils.class.getName()).log(Level.SEVERE, null, e);
				}
				Collections.sort(list, new CollectionUtils());
				if (list.size() <= 5) {
					this.listCar = list;
				} else {
					List<Car> c = new ArrayList<>();
					for (int j = 0; j < 5; j++) {
						c.add(list.get(j));
					}
					this.listCar = c;
				}
				System.out.println("in lần: " + i);

				try {
					WriteFile.ObjectJson(this.listCar);
				} catch (IOException e) {
					Logger.getLogger(ThreadUtils.class.getName()).log(Level.SEVERE, null, e);
				}
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}

		}

	}

}
