package com.car.utils;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.car.dao.CarDao;

import static java.awt.Color.pink;
import static java.awt.Color.white;

public class ValidateUtils {
	
	/*
	 * 1-10 kí tự a-z, A-Z, 0-9
	 */
	public static boolean checkidCar(JTextField txt) {
		txt.setBackground(white);
		String id = txt.getText();
		String rgx = "[a-zA-Z0-9]{1,15}";
		if (id.matches(rgx)) {
			return true;
		} else {
			txt.setBackground(pink);
			JOptionPane.showMessageDialog(txt.getRootPane(),
					txt.getName() + " phải có 1-15 kí tự\nchữ hoa, thường không dấu hoặc số.");
			return false;
		}
	}

	/*
	 * đúng 4 kí tự a-z, A-Z, 0-9
	 */
	public static boolean checkidBrand(JTextField txt) {
		txt.setBackground(white);
		String id = txt.getText();
		String rgx = "[a-zA-Z0-9]{4}";
		if (id.matches(rgx)) {
			return true;
		} else {
			txt.setBackground(pink);
			JOptionPane.showMessageDialog(txt.getRootPane(),
					txt.getName() + " phải có đúng 4 kí tự\nchữ thường, chữ hoa hoặc số");
			return false;
		}
	}

	// gồm các ký tự chữ dấu cách
	// từ 3-25 kí tự
//	public static boolean checkName(JTextField txt) {
//		txt.setBackground(white);
//		String id = txt.getText();
//		String rgx = "^[A-Za-zÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠ-ỹ ]{3,25}$";
//		if (id.matches(rgx)) {
//			return true;
//		} else {
//			txt.setBackground(pink);
//			JOptionPane.showMessageDialog(txt.getRootPane(),
//					txt.getName() + " phải là tên tiếng việt hoặc không đấu\ntừ 3-25 kí tự");
//			return false;
//		}
//	}


	// gio là int >0
	public static boolean checkPrice(JTextField txt) {
		txt.setBackground(white);
		try {
			int hour = Integer.parseInt(txt.getText());
			if (hour > 0) {
				return true;
			} else {
				txt.setBackground(pink);
				JOptionPane.showMessageDialog(txt.getRootPane(), txt.getName() + "phải lớn hơn 0.");
				return false;
			}
		} catch (NumberFormatException e) {
			txt.setBackground(pink);
			JOptionPane.showMessageDialog(txt.getRootPane(), txt.getName() + "phải là số nguyên.");
			return false;
		}
	}

	public static boolean checkNull(JTextField txt) {
		txt.setBackground(white);
		if (txt.getText().trim().length() > 0) {
			return true;
		} else {
			txt.setBackground(pink);
			JOptionPane.showMessageDialog(txt.getRootPane(), "Không được để trống" + txt.getName());
			return false;
		}
	}
	

}
