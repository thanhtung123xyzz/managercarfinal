/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.car.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ASUS
 */

public class Jdbc {

	private static String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	private static String dburl = "jdbc:sqlserver://localhost:1433;databaseName=Carr";
	private static String username = "sa";
	private static String password = "songlong";

	/*
	 * nạp driver
	 * 
	 */
	static {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException ex) {
			throw new RuntimeException(ex);
		}
	}
	/*
	 * Tạo ra PreparedStatement
	 * 
	 * Nạp truyền giá trị đối số vào prepareStatement có thể statment không có đối
	 * Số prepareStatement có thể là prepareStatement hoặc CallableStatements
	 * 
	 * @param sql câu lệnh sql statement (có đối số hoặc không)
	 * 
	 * @param args mảng đối số của câu lệnh sql (có hoặc không) return pstmt là
	 * PrepareStatement đã được truyền đối số
	 */

	public static PreparedStatement prepareStatement(String sql, Object... args) throws SQLException {
		Connection connection = DriverManager.getConnection(dburl, username, password);
		PreparedStatement pstmt = null;
		if (sql.trim().startsWith("{")) {
			pstmt = connection.prepareCall(sql); // Có thể gán biến kiểu PreparedStatement là prepareCall
													// (CallableStatement)
		} else {
			pstmt = connection.prepareStatement(sql);
		}
		for (int i = 0; i < args.length; i++) {
			pstmt.setObject(i + 1, args[i]);
		}
		return pstmt;
	}

	/*
	 * thao tác (INSERT, UPDATE, DELETE) thực thi prepareStatement (đã được truyền
	 * đối số ở hàm trên) khi làm chỉ cần gọi hàm này, từ trong hàm này nó sẽ gọi ra
	 * hàm preparedStatement ở trên
	 * 
	 * @param sql (String) câu lệnh sql statement (có đối số hoặc không)
	 * 
	 * @param args mảng đối số của câu lệnh sql (có hoặc không)
	 */
	public static void executeUpdate(String sql, Object... args) {
		try {

			PreparedStatement stmt = prepareStatement(sql, args);
			try {
				stmt.executeUpdate();
			} finally {
				stmt.getConnection().close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 * Thao tác truy vấn (SELECT) thực thi prepareStatement (đã được truyền đối số ở
	 * Hàm trên) khi làm chỉ cần gọi hàm này, từ trong hàm này nó sẽ gọi ra hàm
	 * PreparedStatement ở trên
	 * 
	 * @param sql (String) câu lệnh sql statement (có đối số hoặc không)
	 * 
	 * @param args mảng đối số của câu lệnh sql (có hoặc không)
	 */
	public static ResultSet executeQuery(String sql, Object... args) {
		try {
			PreparedStatement stmt = prepareStatement(sql, args);
			return stmt.executeQuery();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
// Test db connect
/*
 * public static void main(String args[]) { try { Connection conn =
 * getConnection(dburl, username, password);
 * 
 * Statement stmt = conn.createStatement();
 * 
 * ResultSet rs = stmt.executeQuery("select * from Car");
 * 
 * while (rs.next()) { System.out.println(rs.getInt(1) + "  " + rs.getString(2)
 * + "  " + rs.getInt(3) + " " + rs.getString(4) + " " + rs.getString(5) + "" +
 * rs.getDate(6)); } // close connection conn.close(); } catch (Exception ex) {
 * ex.printStackTrace(); } }
 * 
 * public static Connection getConnection(String dburl, String username, String
 * password) { Connection conn = null; try {
 * Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); conn =
 * DriverManager.getConnection(dburl, username, password);
 * System.out.println("connect successfully!"); } catch (Exception ex) {
 * System.out.println("connect failure!"); ex.printStackTrace(); } return conn;
 * } }
 */
