package com.car.utils;

import java.util.Comparator;

import com.car.model.Car;

public class CollectionUtils implements Comparator<Car> {

	@Override
	public int compare(Car o1, Car o2) {
		if (o1.getPrice() == o2.getPrice()) {
			return 0;
		} else if (o1.getPrice() > o2.getPrice()) {
			return -1;
		} else
			return 1;

	}

}
