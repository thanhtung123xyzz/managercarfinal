package com.car.test;

import static org.junit.Assert.assertEquals;

import javax.annotation.processing.Generated;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.car.model.Brand;

@RunWith(Suite.class)
@Generated(value = "org.junit-tools-1.1.0")
public class BrandTest {

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	private Brand createTestSubject() {
		return new Brand();
	}

	@Test
	public void testGetIdBrand() throws Exception {
		Brand instance = new Brand();
		String exResult = "";
		String result = instance.getIdBrand();
		assertEquals(exResult, result);
	}


	@Test
	public void testSetIdBrand() throws Exception {
		String idBrand = "HD01";
		Brand instance = new Brand();
		instance.setIdBrand(idBrand);
		String expected = "HD01";
		String result = instance.getIdBrand();

		assertEquals(expected, result);
	}


	@Test
	public void testGetNameBrand() throws Exception {
		Brand instance = new Brand();
		String exResult = "";
		String result = instance.getNameBrand();
		
		assertEquals(exResult, result);
	}

	
	@Test
	public void testSetNameBrand() throws Exception {
		String nameBrand = "HONDA";
		Brand instance = new Brand();
		instance.setNameBrand(nameBrand);
		String expected = "HONDA";
		String resul = instance.getNameBrand();
		
		assertEquals(expected, resul);
	}
}