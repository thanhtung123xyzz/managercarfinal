package com.car.test;

import static org.junit.Assert.*;

import javax.annotation.processing.Generated;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.car.model.Car;

//@RunWith(Suite.class)
@Generated(value = "org.junit-tools-1.1.0")
public class CarTest {
	
	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	private Car createTestSubject() {
		return new Car();
	}


	@Test
	public void testGetIdCar() throws Exception {
		Car instance = new Car();
		int exResult = 0;
		int result = instance.getIdCar();

		assertEquals(exResult, result);
	}


	@Test
	public void testSetIdCar() throws Exception {
		int idCar = 1;
        Car instance = new Car(); 	
        int expected= 1;
        instance.setIdCar(idCar);
        int result = instance.getIdCar();

		assertEquals(expected, result);
	}


	@Test
	public void testGetIdBrand() throws Exception {
		Car instance = new Car();
		String exResult = "";
		String result = instance.getIdBrand();
		
		assertEquals(exResult, result);
	}


	@Test
	public void testSetIdBrand() throws Exception {
		String idBrand = "HD01";
		Car instance = new Car();
		String expected = "HD01";
		instance.setIdBrand(idBrand);
		String result = instance.getIdBrand();
		
		assertEquals(expected, result);
	}

	@Test
	public void testGetName() throws Exception {
		Car instance = new Car();
		String expResult = "";
		String result = instance.getName();
		assertEquals(expResult, result);
	}


	@Test
	public void testSetName() throws Exception {
		String nameCar = "LAMBORGHINI";
		Car instance = new Car();
		String expected = "LAMBORGHINI";
		instance.setName(nameCar);
		String result = instance.getName();
		assertEquals(expected, result);
	}


	@Test
	public void testGetPrice() throws Exception {
		Car instance = new Car();
		int exResult = 0;
		int result = instance.getPrice();
		
		assertEquals(exResult, result);
	}


	@Test
	public void testSetPrice() throws Exception {
		int priceCar = 10000000;
		Car instance = new Car();
		int expected = 10000000;
		instance.setPrice(priceCar);
		assertEquals(expected, instance.getPrice());
	}


	@Test
	public void testGetColor() throws Exception {
		Car instance = new Car();
		String exRusult = "";
		String result = instance.getColor();
		assertEquals(exRusult, result);
	}


	@Test
	public void testSetColor() throws Exception {
		String color ="red";
		Car instance = new Car();
		String expected = "red";
		instance.setColor(color);
		String result = instance.getColor();
		assertEquals(expected, result);
	}
}