/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.car.view;

import com.car.dao.BrandDao;
import com.car.dao.CarDao;
import com.car.model.Brand;

import com.car.model.Car;
import com.car.utils.CollectionUtils;
import com.car.utils.ValidateUtils;

import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ASUS
 */

public class CarManagerUI extends javax.swing.JFrame {

	int index = 0;
	CarDao dao = new CarDao();
	BrandDao bdao = new BrandDao();
	private boolean SortS = true;
	private DefaultTableModel model;
	private List<Brand> listBrand = null;

	public CarManagerUI() {
		initComponents();

	}

	/*
	 * load dữ liệu lên table Xóa bảng, đổ toàn bộ khoaHoc từ CSDL vào bảng
	 * Xử lý trong try catch
	 * Gọi ra List brand gán cho câu lệnh truy vấn lấy tất cả dữ liệu
	 * Sử dụng vòng lặp for each để lặp qua list của Car Sau đó gán nó vào Object
	 * rồi gọi lên lên table và hiển thị ra
	 */
	void load() {
		model = (DefaultTableModel) tblGridView.getModel();
		model.setRowCount(0);
		try {
			String keyword = txtTimKiem.getText();
			List<Car> list = dao.selectByKeyword(keyword);
			for (Car c : list) {
				Object[] row = { 
						c.getIdCar(), 
						c.getName(), 
						c.getPrice(), 
						c.getColor(), 
						c.getIdBrand() 
				};
				model.addRow(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/*
	 * Thêm dữ liệu 
	 * Lấy thông tin trên form cho vào đt Car 
	 * Xử lý trong try catch:
	 * Gọi đến truy vấn insert trong CarDao, truyền vào insert đt Car. 
	 * gọi load(); để sau khi thực thi insert sẽ hiển thị dữ liệu mới lên bảng Sau khi
	 * thêm thành công gọi ra clearForm thực hiện xóa trắng form trên các ô text
	 * 
	 */
	void insert() {
		Car model = getModel();
		try {
			dao.insert(model);
			this.load();
			this.clearForm();
			JOptionPane.showMessageDialog(null, "Thêm mới thành công!");
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/*
	 * Cập nhật dữ liệu 
	 * Lấy thông tin trên form cho vào đt Car 
	 * Xử lý trong try catch:
	 * Gọi đến truy vấn update trong CarDao, truyền vào update đt Car. 
	 * gọi load() để sau khi thực thi update sẽ hiển thị dữ liệu mới lên bảng Sau khi
	 * cập nhật thành công gọi ra clearForm thực hiện xóa trắng form trên các ô text
	 * 
	 */
	void update() {
		Car model = getModel();
		try {
			dao.update(model);
			this.clearForm();
			this.load();
			JOptionPane.showMessageDialog(null, "Update thành công!");
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Update thất bại!");
		}
	}

	/*
	 * Xóa dữ liệu 
	 * Xử lý trong try catch:
	 * Gọi đến truy vấn delete trong CarDao, truyền vào  đt Car. 
	 * gọi load() để sau khi thực thi delete sẽ hiển thị dữ liệu mới lên bảng Sau khi
	 * cập nhật thành công gọi ra clearForm thực hiện xóa trắng form trên các ô text
	 * 
	 */
	void delete() {
		Integer mac = Integer.valueOf(txtSTT.getText());
		try {
			dao.delete(mac);
			this.load();
			this.clearForm();
			JOptionPane.showMessageDialog(null, "Xóa thành công!");
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Xóa thất bại!");
		}

	}

	/* 
	 * Xóa trắng các ô text 
	 */
	void clearForm() {
		txtSTT.setText(null);
		txtTenXe.setText(null);
		txtGia.setText(null);
		txtMauXe.setText(null);
		cboThuongHieu.setSelectedIndex(0);
	}
	
	/*
	 * Lấy dữ liệu trên form cho vào đối tượng Car
	 * 
	 */
	Car getModel() {
		Car model = new Car();;
		Brand br = (Brand) cboThuongHieu.getSelectedItem();
		model.setIdBrand(br.getIdBrand());
		model.setName(txtTenXe.getText());
		model.setPrice(Integer.valueOf(txtGia.getText()));
		model.setColor(txtMauXe.getText());
		model.setIdCar(Integer.valueOf(txtSTT.getText()));
		return model;
	}

	/* 
	 * Truyền dữ liệu vào combobox
	 */
	void FillComboBox() {
		DefaultComboBoxModel model = (DefaultComboBoxModel) cboThuongHieu.getModel(); // kết nối model với cbo
		model.removeAllElements();
		try {
			listBrand = bdao.select();
			for (Brand cd : listBrand) {
				model.addElement(cd); // thêm đối tượng (Object) vào model
				// chỉ thêm đc đối tượng đối với model, cbo chỉ được cbo.addItem(String);
				// lấy đối tượng thì từ cbo cũng được: cbo.getSelectedItem();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * hiện thị dữ liệu lên các ô text khi thực thi MouseClick vào table
	 * 
	 */
	void textFill(int index) {
		txtSTT.setText(tblGridView.getValueAt(index, 0).toString());
		txtTenXe.setText(tblGridView.getValueAt(index, 1).toString());
		txtGia.setText(tblGridView.getValueAt(index, 2).toString());
		txtMauXe.setText(tblGridView.getValueAt(index, 3).toString());
		for (int i = 0; i < listBrand.size(); i++) {
			if (listBrand.get(i).getNameBrand().equals(tblGridView.getValueAt(index, 4))) {
				cboThuongHieu.setSelectedItem(listBrand.get(i));
			}
		}
	}

	void close() {
		WindowEvent closeWindow = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(closeWindow);
	}

	/* Sắp xếp theo thứ tự giá cao
	 */
	void Sort() {
		model = (DefaultTableModel) tblGridView.getModel();
		List<Car> list = dao.select();
		if (SortS) {
			Collections.sort(list, new CollectionUtils());

			model = (DefaultTableModel) tblGridView.getModel();
			model.setRowCount(0);
			int first = 0;
			for (Car c : list) {
				Object[] row = { 
						c.getIdCar(),
						c.getName(),
						c.getPrice(),
						c.getColor(),
						c.getIdBrand()
				};
				model.addRow(row);
			}
		} else {
			load();
		}
		SortS = !SortS;
	}

	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated
	// <editor-fold defaultstate="collapsed" desc="Generated
	// <editor-fold defaultstate="collapsed" desc="Generated
	// Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		buttonGroup1 = new javax.swing.ButtonGroup();
		jLabel1 = new javax.swing.JLabel();
		lblTenXe = new javax.swing.JLabel();
		lblGia = new javax.swing.JLabel();
		lblSDT = new javax.swing.JLabel();
		lblThuongHieu = new javax.swing.JLabel();
		txtTenXe = new javax.swing.JTextField();
		txtGia = new javax.swing.JTextField();
		txtMauXe = new javax.swing.JTextField();
		btnCreate = new javax.swing.JButton();
		btnDelete = new javax.swing.JButton();
		btnUpdate = new javax.swing.JButton();
		jScrollPane2 = new javax.swing.JScrollPane();
		tblGridView = new javax.swing.JTable();
		btnClear = new javax.swing.JButton();
		btnGoBack = new javax.swing.JButton();
		txtTimKiem = new javax.swing.JTextField();
		btnTimKiem = new javax.swing.JButton();
		cboThuongHieu = new javax.swing.JComboBox<>();
		jLabel2 = new javax.swing.JLabel();
		txtSTT = new javax.swing.JTextField();
		btn5xeGiaCao = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
		jLabel1.setForeground(new java.awt.Color(0, 204, 0));
		jLabel1.setText("CAR MANAGER");
		jLabel1.setToolTipText("");
		jLabel1.setPreferredSize(new java.awt.Dimension(100, 30));

		lblTenXe.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
		lblTenXe.setText("Tên xe");

		lblGia.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
		lblGia.setText("Giá xe");

		lblSDT.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
		lblSDT.setText("Màu xe");

		lblThuongHieu.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
		lblThuongHieu.setText("Tên Hãng");

		txtTenXe.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				txtTenXeActionPerformed(evt);
			}
		});

		txtGia.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				txtGiaActionPerformed(evt);
			}
		});

		btnCreate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Images/Add.png"))); // NOI18N
		btnCreate.setText("Create");
		btnCreate.setMaximumSize(new java.awt.Dimension(99, 33));
		btnCreate.setMinimumSize(new java.awt.Dimension(99, 33));
		btnCreate.setPreferredSize(new java.awt.Dimension(99, 33));
		btnCreate.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnCreateActionPerformed(evt);
			}
		});

		btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Images/Delete.png"))); // NOI18N
		btnDelete.setText("Delete");
		btnDelete.setMaximumSize(new java.awt.Dimension(99, 33));
		btnDelete.setMinimumSize(new java.awt.Dimension(99, 33));
		btnDelete.setPreferredSize(new java.awt.Dimension(99, 33));
		btnDelete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnDeleteActionPerformed(evt);
			}
		});

		btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Images/Edit.png"))); // NOI18N
		btnUpdate.setText("Update");
		btnUpdate.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnUpdateActionPerformed(evt);
			}
		});

		tblGridView.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {

		}, new String[] { "STT", "Tên Xe", "Giá Xe", "Màu Xe", "Tên Hãng" }));
		tblGridView.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tblGridViewMouseClicked(evt);
			}
		});
		jScrollPane2.setViewportView(tblGridView);

		btnClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Images/No.png"))); // NOI18N
		btnClear.setText("Clear");
		btnClear.setMaximumSize(new java.awt.Dimension(99, 33));
		btnClear.setMinimumSize(new java.awt.Dimension(99, 33));
		btnClear.setPreferredSize(new java.awt.Dimension(99, 33));
		btnClear.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnClearActionPerformed(evt);
			}
		});

		btnGoBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Images/Refresh.png"))); // NOI18N
		btnGoBack.setText("Go Brand");
		btnGoBack.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnGoBackActionPerformed(evt);
			}
		});

		btnTimKiem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Images/Zoom.png"))); // NOI18N
		btnTimKiem.setText("Find");
		btnTimKiem.setPreferredSize(new java.awt.Dimension(90, 33));
		btnTimKiem.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnTimKiemActionPerformed(evt);
			}
		});

		cboThuongHieu.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cboThuongHieuActionPerformed(evt);
			}
		});

		jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
		jLabel2.setText("STT");

		btn5xeGiaCao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Images/Best.png"))); // NOI18N
		btn5xeGiaCao.setText("Sort price");
		btn5xeGiaCao.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btn5xeGiaCaoActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGroup(layout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jScrollPane2)
						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
								.addGap(56, 56, 56)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
										.addGroup(layout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
												.addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 196,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGap(179, 179, 179).addComponent(btnGoBack))
										.addGroup(javax.swing.GroupLayout.Alignment.LEADING,
												layout.createSequentialGroup().addGroup(layout
														.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(layout.createSequentialGroup().addGroup(layout
																.createParallelGroup(
																		javax.swing.GroupLayout.Alignment.LEADING)
																.addGroup(layout.createSequentialGroup()
																		.addGroup(layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																				.addComponent(lblSDT)
																				.addComponent(lblThuongHieu))
																		.addGap(59, 59, 59))
																.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
																		layout.createSequentialGroup()
																				.addGroup(layout.createParallelGroup(
																						javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(lblTenXe)
																						.addComponent(lblGia)
																						.addComponent(jLabel2))
																				.addGap(71, 71, 71)))
																.addGroup(layout.createParallelGroup(
																		javax.swing.GroupLayout.Alignment.LEADING)
																		.addGroup(layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING,
																				false)
																				.addComponent(txtMauXe,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						207, Short.MAX_VALUE)
																				.addComponent(cboThuongHieu, 0,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE))
																		.addGroup(
																				javax.swing.GroupLayout.Alignment.TRAILING,
																				layout.createParallelGroup(
																						javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(txtSTT,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								207,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(txtGia,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								207,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(txtTenXe,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								207,
																								javax.swing.GroupLayout.PREFERRED_SIZE)))
																.addGap(29, 29, 29)
																.addGroup(layout.createParallelGroup(
																		javax.swing.GroupLayout.Alignment.TRAILING)
																		.addComponent(btnCreate,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(btnUpdate,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				99,
																				javax.swing.GroupLayout.PREFERRED_SIZE)))
														.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout
																.createSequentialGroup().addComponent(txtTimKiem)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																.addComponent(btnTimKiem,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.PREFERRED_SIZE)))
														.addGap(36, 36, 36)
														.addGroup(layout
																.createParallelGroup(
																		javax.swing.GroupLayout.Alignment.LEADING)
																.addGroup(
																		layout.createSequentialGroup()
																				.addComponent(
																						btn5xeGiaCao)
																				.addGap(60, 60, 60))
																.addGroup(layout.createSequentialGroup().addGroup(layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(btnDelete,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(btnClear,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addGap(0, 0, Short.MAX_VALUE)))))))
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap().addGroup(
						layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(btnGoBack))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jLabel2)
						.addComponent(txtSTT, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(
						layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(
										txtTenXe, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(lblTenXe))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addGroup(layout
						.createSequentialGroup()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(txtGia, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(lblGia))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(txtMauXe, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(lblSDT))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblThuongHieu).addComponent(cboThuongHieu,
										javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(47, 47, 47))
						.addGroup(layout.createSequentialGroup().addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(btnUpdate).addComponent(btnDelete,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(42, 42, 42)))
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(txtTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addComponent(btnTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addComponent(btn5xeGiaCao))
				.addGap(18, 18, 18)
				.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 164,
						javax.swing.GroupLayout.PREFERRED_SIZE)
				.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		pack();
	}// </editor-fold>//GEN-END:initComponents

	private void btn5xeGiaCaoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btn5xeGiaCaoActionPerformed
		this.Sort();
	}// GEN-LAST:event_btn5xeGiaCaoActionPerformed

	private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCreateActionPerformed
		// TODO add your handling code here:
		if (ValidateUtils.checkNull(txtSTT) 
				&& ValidateUtils.checkNull(txtTenXe) 
				&& ValidateUtils.checkNull(txtGia)
				&& ValidateUtils.checkNull(txtMauXe)) {
			if (ValidateUtils.checkidCar(txtSTT) 
					&& ValidateUtils.checkPrice(txtGia))
				this.insert();
		}

	}// GEN-LAST:event_btnCreateActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnDeleteActionPerformed
		// TODO add your handling code here:
		this.delete();
	}// GEN-LAST:event_btnDeleteActionPerformed

	private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnUpdateActionPerformed
		if (ValidateUtils.checkNull(txtSTT) 
				&& ValidateUtils.checkNull(txtTenXe) 
				&& ValidateUtils.checkNull(txtGia)
				&& ValidateUtils.checkNull(txtMauXe)
				&& ValidateUtils.checkPrice(txtGia)) 	
				this.update();
	}// GEN-LAST:event_btnUpdateActionPerformed

	private void tblGridViewMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_tblGridViewMouseClicked
		index = tblGridView.getSelectedRow();
		this.textFill(index);
	}// GEN-LAST:event_tblGridViewMouseClicked

	private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnClearActionPerformed
		this.clearForm();
	}// GEN-LAST:event_btnClearActionPerformed

	private void txtTenXeActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_txtTenXeActionPerformed

	}// GEN-LAST:event_txtTenXeActionPerformed

	private void txtGiaActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_txtGiaActionPerformed

	}// GEN-LAST:event_txtGiaActionPerformed

	private void btnGoBackActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnGoBackActionPerformed
		close();
		// trở về form brand
		BrandUI brUI = new BrandUI();
		brUI.setVisible(true);
	}// GEN-LAST:event_btnGoBackActionPerformed

	private void formWindowOpened(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_formWindowOpened
		this.setLocationRelativeTo(null);
		this.load();
		this.FillComboBox();
	}// GEN-LAST:event_formWindowOpened

	private void cboThuongHieuActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cboThuongHieuActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_cboThuongHieuActionPerformed

	private void btnTimKiemActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnTimKiemActionPerformed
		// TODO add your handling code here:
		this.load();
	}// GEN-LAST:event_btnTimKiemActionPerformed

	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
		// (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default
		 * look and feel. For details see
		 * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;

				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(CarManagerUI.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);

		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(CarManagerUI.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);

		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(CarManagerUI.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);

		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(CarManagerUI.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		}
		// </editor-fold>
		// </editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new CarManagerUI().setVisible(true);

			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton btn5xeGiaCao;
	private javax.swing.JButton btnClear;
	private javax.swing.JButton btnCreate;
	private javax.swing.JButton btnDelete;
	private javax.swing.JButton btnGoBack;
	private javax.swing.JButton btnTimKiem;
	private javax.swing.JButton btnUpdate;
	private javax.swing.ButtonGroup buttonGroup1;
	private javax.swing.JComboBox<String> cboThuongHieu;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JLabel lblGia;
	private javax.swing.JLabel lblSDT;
	private javax.swing.JLabel lblTenXe;
	private javax.swing.JLabel lblThuongHieu;
	private javax.swing.JTable tblGridView;
	private javax.swing.JTextField txtGia;
	private javax.swing.JTextField txtMauXe;
	private javax.swing.JTextField txtSTT;
	private javax.swing.JTextField txtTenXe;
	private javax.swing.JTextField txtTimKiem;
	// End of variables declaration//GEN-END:variables

}
