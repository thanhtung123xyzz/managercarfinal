/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.car.dao;

import com.car.model.Brand;
import com.car.utils.Jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class BrandDao implements InterfaceController<Brand, String> {
	
	/*
	 * Thêm mới thực thể vào CSDL
	 * 
	 * @param model chứa thông tin của bản ghi mới
	 */
	@Override
	public void insert(Brand model) {
		String sql = "insert into Brand (idBrand, NameBrand)" + " values(?,?)";
		Jdbc.executeUpdate(sql, model.getIdBrand(), model.getNameBrand());

	}

	/*
	 * Cập nhật thực thể vào CSDL
	 * 
	 * @param model chưa thông tin bản ghi mới cập nhật
	 */
	@Override
	public void update(Brand model) {
		String sql = "UPDATE Brand SET NameBrand=? WHERE idBrand=?";
		Jdbc.executeUpdate(sql, model.getNameBrand(), model.getIdBrand());

	}
	
	/*
	 * Xóa bản ghi khỏi CSDL
	 * 
	 * @param idBrand là id cần xóa
	 */
	@Override
	public void delete(String idBrand) {
		String sql = "delete from Brand where idBrand=?";
		Jdbc.executeUpdate(sql, idBrand);

	}
	
	/*
	 * Truy vấn tất cả các dữ liệu trong Brand
	 * 
	 */
	@Override
	public List<Brand> select() {
		String sql = "select * from Brand";
		return select(sql);
	}

	@Override
	public List<Brand> selectByKeyword(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*
	 * Truy vấn dữ liệu theo idCar
	 * 
	 * @param idCar là mã dữ liệu được truy vấn
	 * 
	 * @return thực thể chứa thông tin của bản ghi
	 */
	@Override
	public Brand findById(String idBrand) {
		String sql = "SELECT * FROM Brand WHERE idBrand=?";
		List<Brand> list = select(sql, idBrand);
		return list.size() > 0 ? list.get(0) : null;
	}
	
	/*
	 * Thực hiện truy vấn lấy về 1 tập ResultSet rồi điền tập ResultSet đó vào 1
	 * List
	 * 
	 */
	@Override
	public List<Brand> select(String sql, Object... args) {
		List<Brand> list = new ArrayList<>();
		try {
			ResultSet rs = null;
			try {
				rs = Jdbc.executeQuery(sql, args);
				while (rs.next()) {
					Brand model = readFromResultSet(rs);
					list.add(model);
				}
			} finally {
				rs.getStatement().getConnection().close();
			}
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return list;
	}

	/*
	 * đọc 1 Brand từ 1 bản ghi (1 ResultSet)
	 * 
	 */
	public Brand readFromResultSet(ResultSet rs) throws SQLException {
		Brand model = new Brand();
		model.setIdBrand(rs.getString("idBrand"));
		model.setNameBrand(rs.getString("NameBrand"));
		return model;
	}

}
