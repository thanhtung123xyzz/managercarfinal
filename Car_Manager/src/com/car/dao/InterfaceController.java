package com.car.dao;

import java.util.List;


public interface InterfaceController<T, V> {
	
	public abstract void insert(T model);
	
	public abstract void update(T model);
	
	public abstract void delete(V v);
	
	public abstract List<T> select();
	
	public abstract List<T> selectByKeyword(String keyword);
	
	public abstract T findById(V v);
	
	public abstract List<T> select(String sql, Object... args);
	
	
	
	

}
