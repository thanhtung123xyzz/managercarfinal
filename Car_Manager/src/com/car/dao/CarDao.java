/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.car.dao;

import com.car.model.Car;
import com.car.utils.Jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class CarDao implements InterfaceController<Car, Integer> {

	

	/*
	 * Thêm mới thực thể vào CSDL
	 * 
	 * @param model chứa thông tin của bản ghi mới
	 */
	@Override
	public void insert(Car model) {
		String sql = "insert into car (idCar, idBrand, name, price, color)" + " values(?,?,?,?,?)";
		Jdbc.executeUpdate(sql, model.getIdCar(), model.getIdBrand(), model.getName(), model.getPrice(),
				model.getColor());

	}

	@Override
	public void update(Car model) {
		String sql = "update car set idBrand=?, name=?, price=?, color=? where idCar=?";
		Jdbc.executeUpdate(sql, model.getIdBrand(), model.getName(), model.getPrice(), model.getColor(),
				model.getIdCar());

	}

	@Override
	public void delete(Integer idCar) {
		String sql = "delete from Car where idCar=?";
		Jdbc.executeUpdate(sql, idCar);

	}

	@Override
	public List<Car> select() {
		String sql = "select idcar, Name, price, color, namebrand from Car join "
				+ "Brand on Car.idBrand = Brand.idBrand";
		return select(sql);
	}

	@Override
	public List<Car> selectByKeyword(String keyword) {
		String sql = "select idcar, Name, price, color, namebrand from Car join "
				+ "Brand on Car.idBrand = Brand.idBrand WHERE name LIKE ?";
		return select(sql, "%" + keyword + "%");
	}

	@Override
	public Car findById(Integer idCar) {
		String sql = "SELECT * FROM car WHERE idCar=?";
		List<Car> list = select(sql, idCar);
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	public List<Car> select(String sql, Object... args) {
		List<Car> list = new ArrayList<>();
		try {
			ResultSet rs = null;
			try {
				rs = Jdbc.executeQuery(sql, args);
				while (rs.next()) {
					list.add(readFromResultSet(rs));
				}
			} finally {
				rs.getStatement().getConnection().close();
			}
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return list;
	}

	/*
	 * đọc 1 Car từ 1 bản ghi (1 ResultSet)
	 * 
	 */
	public Car readFromResultSet(ResultSet rs) throws SQLException {
		Car model = new Car();
		model.setIdCar(rs.getInt("idCar"));
		model.setName(rs.getString("name"));
		model.setPrice(rs.getInt("price"));
		model.setColor(rs.getString("color"));
		model.setIdBrand(rs.getString("namebrand"));
		return model;
	}

}
