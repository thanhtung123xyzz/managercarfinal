/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.car.model;





public class Car {
	
    private int idCar;
    private String idBrand;
    private String name;
    private int price;
    private String color;

    public Car() {
    }

    public Car(int idCar, String idBrand, String name, int price, String color) {
        this.idCar = idCar;
        this.idBrand = idBrand;
        this.name = name;
        this.price = price;
        this.color = color;
    }

    public int getIdCar() {
        return idCar;
    }

    public void setIdCar(int idCar) {
        this.idCar = idCar;
    }

    public String getIdBrand() {
        return idBrand;
    }

    public void setIdBrand(String idBrand) {
        this.idBrand = idBrand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return this.idBrand;
    }

}
